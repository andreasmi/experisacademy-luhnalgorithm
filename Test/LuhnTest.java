import com.experis.academy.andreas.Luhn.Luhn;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class LuhnTest {
    Luhn luhn = new Luhn();
    @Test
    public void isNumber16digits(){
        assertTrue(luhn.is16digits("1234567890123456"));
        assertFalse(luhn.is16digits("14567890123456"));
        assertFalse(luhn.is16digits("1212334567890123456"));
        assertTrue(luhn.is16digits("123456789a1234hg"));
    }

    @Test
    public void isStringOnlyIntegers(){
        assertTrue(luhn.isNumberInt("12312312312312"));
        assertTrue(luhn.isNumberInt("1231231239740234792834712312"));
        assertFalse(luhn.isNumberInt("1sv!.-23123122"));
        assertFalse(luhn.isNumberInt("-.-"));
    }

    @Test
    public void makeProductByAddingEverySecondNumberBy2(){
        assertEquals("2264165890226416", luhn.makeProductNumber("1234567890123456"));
        assertNotEquals("2264165890224856", luhn.makeProductNumber("1234567890123456"));
    }

    @Test
    public void everyNumberAddedTogetherExceptChecsum(){
        assertEquals("586", luhn.addEveryNumberExceptChecksum("2264165890226416"));
        assertNotEquals("646", luhn.addEveryNumberExceptChecksum("2264165890226416"));
        assertEquals("646", luhn.addEveryNumberExceptChecksum("2264165890224856"));
    }

    @Test
    public void checkIfNumberIsALuhnNumber(){
        assertTrue(luhn.isNumberLuhn("582"));
        assertFalse(luhn.isNumberLuhn("586"));
    }

    @Test
    public void extractChecksumFromNumber(){

        ArrayList<String> expected = new ArrayList<>();
        ArrayList<String> actaul = luhn.extractChecksum("1234567890123456");

        expected.add("123456789012345");
        expected.add("6");

        assertEquals(expected, actaul);
    }
}