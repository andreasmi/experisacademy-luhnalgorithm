package com.experis.academy.andreas.Luhn;

import com.experis.academy.andreas.ConsoleHelper.Color;
import com.experis.academy.andreas.ConsoleHelper.ConsoleHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Luhn {

    public void doLuhn(String number){
        if(!isNumberInt(number)){
            ConsoleHelper.printErrorAndExit("You can only enter integers", 1);
        }
        if(!is16digits(number)){
            ConsoleHelper.printErrorAndExit("You need to have 16 digits for it to be a credit card", 1);
        }

        ConsoleHelper.printMessage("Your card number and checksum: ");
        ConsoleHelper.printNumberAndChecksum(extractChecksum(number), Color.BLUE);

        String product = makeProductNumber(number);
        String sumOfEveryDigitWithChecksumAtEnd = addEveryNumberExceptChecksum(product);
        Boolean isNumberLuhn = isNumberLuhn(sumOfEveryDigitWithChecksumAtEnd);

        if (isNumberLuhn) {
            ConsoleHelper.printMessage("VALID", Color.GREEN);
        } else {
            ConsoleHelper.printMessage("INVALID", Color.RED);
        }

    }

    //Multiplying every other number på 2
    public String makeProductNumber(String number){
        String[] numberArray = number.split("");
        for(int i = numberArray.length-2; i >= 0; i-=2){
            int currentNumber = Integer.parseInt(numberArray[i]) * 2;
            int newNumber;
            if(String.valueOf(currentNumber).length() > 1){
                String[] singleNumberArray = String.valueOf(currentNumber).split("");
                newNumber = Integer.parseInt(singleNumberArray[0])+Integer.parseInt(singleNumberArray[1]);
            }else{
                newNumber = currentNumber;
            }
            numberArray[i] = String.valueOf(newNumber);
        }

        return String.join("",numberArray);
    }

    //Splitting number, adding everyone togheter, reducing by the checsum and
    //adding checsum to the end as a string
    public String addEveryNumberExceptChecksum(String number){
        List<String> numberArray = Arrays.asList(number.split(""));
        String checksum = numberArray.get(numberArray.size()-1);
        return numberArray.stream().map(Integer::parseInt).reduce(0, Integer::sum) - Integer.parseInt(checksum) + checksum;
    }

    //Slits up the number, adding them altogehther except the last one
    //Calculating the correct checksum and the comparing it
    public Boolean isNumberLuhn(String number){
        String[] numberArray = number.split("");
        String actualNumber = "";
        for(int i = 0; i < numberArray.length-1; i++){
            actualNumber += numberArray[i];
        }

        int actualCheckDigit = (Integer.parseInt(actualNumber)*9)%10;
        ConsoleHelper.printMessage("Checking " + actualCheckDigit + " against " + numberArray[numberArray.length-1],Color.YELLOW);

        return actualCheckDigit==Integer.parseInt(numberArray[numberArray.length-1]);
    }

    //Here I split the number, adding everyone to a string except the last on
    //Then addind the string to the result and the checsum to the result array
    public ArrayList<String> extractChecksum(String number){
        ArrayList<String> result = new ArrayList<>();
        String[] numberArray = number.split("");
        String numberWihtoutChecksum = "";
        for(int i = 0; i < numberArray.length-1; i++){
            numberWihtoutChecksum += numberArray[i];
        }

        result.add(numberWihtoutChecksum);
        result.add(numberArray[numberArray.length-1]);
        return result;

    }

    public Boolean is16digits(String number){
        return number.length()==16;
    }

    public Boolean isNumberInt(String number){
        try{
            //Needs to split it up sinces the input is too long to be parsed as integer
            String[] numberString = number.split("");
            for(String num : numberString){
                Integer.parseInt(num);
            }
            return true;
        }catch (NumberFormatException ex){
            return false;
        }
    }

}
