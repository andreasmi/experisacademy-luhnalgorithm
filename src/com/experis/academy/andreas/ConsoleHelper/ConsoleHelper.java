package com.experis.academy.andreas.ConsoleHelper;

import java.util.ArrayList;

public class ConsoleHelper {
    public static void printErrorAndExit(String message, int exitCode){
        System.out.println(Color.RED+message+Color.RESET + "\n");
        System.exit(exitCode);
    }

    public static void printMessage(String message, String color){
        System.out.println("\n" + color+message+Color.RESET);
    }
    public static void printMessage(String message){
        System.out.println("\n" + message);
    }
    public static void printNumberAndChecksum(ArrayList<String> numbers, String color){
        System.out.println(color+numbers.get(0)+" "+numbers.get(1) + Color.RESET);
    }
    public static void printSeperator(){
        System.out.println("________________________________________");
    }
}
