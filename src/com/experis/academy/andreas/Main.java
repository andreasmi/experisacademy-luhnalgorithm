package com.experis.academy.andreas;

import com.experis.academy.andreas.ConsoleHelper.ConsoleHelper;
import com.experis.academy.andreas.Luhn.Luhn;

public class Main {

    public static void main(String[] args) {
        if(args.length != 1){
            System.exit(1);
        }

        Luhn luhn = new Luhn();
        luhn.doLuhn(args[0]);
    }
}
